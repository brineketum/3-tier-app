include:
  - template: Security/SAST.gitlab-ci.yml
  - template: "Workflows/MergeRequest-Pipelines.gitlab-ci.yml" # https://docs.gitlab.com/ee/ci/yaml/workflow.html#workflowrules-templates

# configure cross build cache
cache:
  - key: defualt
    paths:
      ## Cache for yarn packages
      - $YARN_CACHE_DIR

  - key: ${CI_COMMIT_REF_SLUG} # cache node_modules folder
    paths:
      - node_modules/

variables:
  RUN_ENVS:
    value: "dev" # Deploy to dev by default
    description: "The deployment target. Change this variable to 'dev' or 'qa' or deploy multiple 'dev qa' if needed."
  RUN_TIERS:
    value: "infra" # Deploy app tier by default
    description: "The deployment tier, Change this variable to 'api', 'infra', 'web' or multiple 'api infra web' if needed."
  TIER_IMAGE_TAG:
    description: "A tier image tag to push to registry, could be same as deploy, so it's deployed."
  BUILD_TIER_IMAGE:
    description: "If tier image should be built. Otherwise would deploy tag or ci."
  BUILD_CI_IMAGE:
    description: "If set to 'yes', will build a new CI Runner Image, otherwise would not."

  # https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_TLS_VERIFY: 1
  DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"
  YARN_CACHE_DIR: ".cache/yarn"
  CI_IMAGE_TAG: "latest"
  APP_NAME: "kbrine-3tier"

stages:
  - pre

  - check
  - test
  - check-build

  - plan
  - apply

  - pre-build
  - build
  - deploy

# Environment
.api-dev:
  {
    variables:
      {
        ENV_NAME: dev,
        TIER: api,
        AWS_REGION: $AWS_REGION_DEV,
        AUTO_DEPLOY: "true",
      },
  }
.api-prod:
  {
    variables:
      {
        ENV_NAME: prod,
        TIER: api,
        AWS_REGION: "us-east-2",
        AUTO_DEPLOY: "true",
      },
  }

.web-dev:
  {
    variables:
      {
        ENV_NAME: dev,
        TIER: web,
        AWS_REGION: $AWS_REGION_DEV,
        AUTO_DEPLOY: "true",
      },
  }
.web-prod:
  {
    variables:
      {
        ENV_NAME: prod,
        TIER: web,
        AWS_REGION: "us-east-2",
        AUTO_DEPLOY: "true",
      },
  }

.infra-dev:
  { variables: { ENV_NAME: dev, TIER: infra, AWS_REGION: $AWS_REGION_DEV } }
.infra-prod:
  { variables: { ENV_NAME: prod, TIER: infra, AWS_REGION: "us-east-2" } }

image: $CI_REGISTRY_IMAGE:$CI_IMAGE_TAG

# not when from pipeline
.master: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"

.infra-changedfiles:
  - .gitlab-ci.yml
  - .gitignore
  - Dockerfile
  - infra/modules/**/*
  - infra/live/${ENV_NAME}/**/*

.changedfiles:
  - .gitlab-ci.yml
  - .gitignore
  - Dockerfile
  - ${TIER}/**/*

.build-image-base:
  services:
    - docker:dind
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  before_script:
    - amazon-linux-extras install docker
    - yum -y install jq
    - aws --version
    - docker --version

build-upload-ci:
  extends: [.build-image-base]
  stage: pre
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_PIPELINE_IID
    IMAGE_TAG_LATEST: $CI_REGISTRY_IMAGE
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
    - echo $IMAGE_TAG
    - docker tag $IMAGE_TAG $IMAGE_TAG_LATEST
    - docker push $IMAGE_TAG_LATEST
  rules:
    - if: $BUILD_CI_IMAGE == 'yes' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH # if true will build the CI Image
    - if: $CI_PIPELINE_SOURCE != "web" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - Dockerfile

.tf-check:
  stage: check
  rules:
    - if: $TIER =~ $RUN_TIERS && $ENV_NAME =~ $RUN_ENVS # from console
    - if: $CI_PIPELINE_SOURCE != "web" && "$CI_COMMIT_BRANCH"
      changes: !reference [.infra-changedfiles]

.tf-format-base:
  extends: [.tf-check]
  script:
    - cd $TIER/live/$ENV_NAME
    - tfswitch
    - terraform fmt -diff -recursive -check

.tf-validate-base:
  extends: [.tf-check]
  script:
    - cd $TIER/live/$ENV_NAME
    - tfswitch
    - terraform init -backend=false -force-copy -reconfigure
    - terraform validate

.tf-tflint-base:
  extends: [.tf-check]
  script:
    - tflint --version
    - cd $TIER/live/$ENV_NAME
    - tfswitch
    - terraform init -backend=false -force-copy -reconfigure
    - tflint --format=junit --var-file=terraform.tfvars . > tflint.xml
  artifacts:
    reports:
      junit:
        - $TIER/live/$ENV_NAME/tflint.xml

.tf-tfsec-base:
  extends: [.tf-check]
  allow_failure: true
  script:
    - tfsec --version
    - cd $TIER/live/$ENV_NAME
    - tfswitch
    - terraform init -backend=false -force-copy -reconfigure
    - tfsec --format=default,junit --tfvars-file=terraform.tfvars -O tfsec .
  artifacts:
    reports:
      junit:
        - $TIER/live/$ENV_NAME/tfsec.junit

.test-base:
  stage: test
  script:
    - cd $TIER
    - yarn install --cache-folder $YARN_CACHE_DIR
    - yarn run test
  rules:
    - if: $TIER =~ $RUN_TIERS && $ENV_NAME =~ $RUN_ENVS
    - if: $CI_PIPELINE_SOURCE != "web" && "$CI_COMMIT_BRANCH" && $AUTO_DEPLOY
      changes: !reference [.changedfiles]

.set-common-vars:
  script:
    - ECR_REPOSITORY_URL=$(aws ecr describe-repositories --region "${AWS_REGION}" --repository-names $APP_NAME-$TIER | jq '.repositories[0].repositoryUri' | sed 's/"//g')
    - TAG="$([ ! -z "$TIER_IMAGE_TAG" ] && echo "$TIER_IMAGE_TAG" || echo $CI_COMMIT_SHORT_SHA)"
    - IMAGE_TAG=$ECR_REPOSITORY_URL:$TAG
    - IMAGE_TAG_LATEST=$ECR_REPOSITORY_URL

.build-tier-image:
  extends: [.build-image-base]
  script:
    - !reference [.set-common-vars, script]
    - cd $TIER
    - docker build -t $IMAGE_TAG .

.check-build-base:
  extends: [.build-image-base]
  stage: check-build
  script:
    - docker build -t test-build-$CI_PIPELINE_ID .
  rules:
    - if: ($CI_COMMIT_BRANCH || ($TIER =~ $RUN_TIERS)) && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: !reference [.changedfiles]

.build-upload-base:
  extends: [.build-tier-image]
  stage: build
  script:
    - !reference [.build-tier-image, script]
    - aws ecr get-login-password --region $AWS_REGION | docker login --username AWS --password-stdin $(aws sts get-caller-identity | jq '.Account' | sed 's/"//g').dkr.ecr.$AWS_REGION.amazonaws.com
    - docker push $IMAGE_TAG
    - docker tag $IMAGE_TAG $IMAGE_TAG_LATEST
    - docker push $IMAGE_TAG_LATEST
  rules:
    - if: $TIER =~ $RUN_TIERS && $ENV_NAME =~ $RUN_ENVS && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $BUILD_TIER_IMAGE
    - if: $CI_PIPELINE_SOURCE != "web" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $AUTO_DEPLOY
      changes: !reference [.changedfiles]

.deploy-base:
  stage: deploy
  script:
    - !reference [.set-common-vars, script]
    - CLUSTER_NAME="$APP_NAME-$ENV_NAME"
    - SERVICE_NAME="$APP_NAME-$TIER-$ENV_NAME"
    - REPOSITORY_NAME="$APP_NAME-$TIER"
    - TASK_DEFINITION_NAME="$APP_NAME-$TIER-$ENV_NAME"

    - echo "check to see if image exist ($IMAGE_TAG) exists"
    - IMAGE_META="$(aws ecr describe-images --repository-name=$REPOSITORY_NAME --region "${AWS_REGION}" --image-ids=imageTag=$TAG 2> /dev/null)"

    - TASK_DEFINITION=$(aws ecs describe-task-definition --task-definition "$TASK_DEFINITION_NAME" --region "${AWS_REGION}")
    - echo $TASK_DEFINITION | jq ".taskDefinition.containerDefinitions[0].image = \"$IMAGE_TAG\"" | jq ".taskDefinition" | jq "del(.taskDefinitionArn, .revision, .status, .requiresAttributes, .compatibilities, .registeredAt, .registeredBy)" > def.json

    - echo "Registering new container definition..."
    - aws ecs register-task-definition --region "${AWS_REGION}" --cli-input-json file://def.json

    - echo "Updating the service..."
    - aws ecs update-service --region "${AWS_REGION}" --cluster "${CLUSTER_NAME}" --service "${SERVICE_NAME}"  --task-definition "${TASK_DEFINITION_NAME}"
  rules:
    - if: $TIER =~ $RUN_TIERS && $ENV_NAME =~ $RUN_ENVS && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $TIER_IMAGE_TAG
    - if: $CI_PIPELINE_SOURCE != "web" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $AUTO_DEPLOY
      changes: !reference [.changedfiles]

.tf-base:
  variables:
    PLAN: plan.cache
    PLAN_JSON: plan.json
    TF_ENV_DIR: $TIER/live/$ENV_NAME
  before_script:
    - cd $TF_ENV_DIR
    - tfswitch
    - terraform init

.tf-plan-base:
  extends: [.tf-base]
  stage: plan
  script:
    - terraform plan -out=$PLAN
    - terraform show --json $PLAN | jq -r '([.resource_changes[]?.change.actions?]|flatten)|{"create":(map(select(.=="create"))|length),"update":(map(select(.=="update"))|length),"delete":(map(select(.=="delete"))|length)}' > $PLAN_JSON
  artifacts:
    paths:
      - $TF_ENV_DIR/$PLAN
    reports:
      terraform: $TF_ENV_DIR/$PLAN_JSON
  rules:
    - if: $TIER =~ $RUN_TIERS && $ENV_NAME =~ $RUN_ENVS # from console
    - if: $CI_PIPELINE_SOURCE != "web" && "$CI_COMMIT_BRANCH"
      changes: !reference [.infra-changedfiles]

.tf-apply-base:
  extends: [.tf-base]
  stage: apply
  script:
    - terraform apply $PLAN
  rules:
    - if: $TIER =~ $RUN_TIERS && $ENV_NAME =~ $RUN_ENVS && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH # from console
      when: manual
    - if: $CI_PIPELINE_SOURCE != "web" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $AUTO_DEPLOY # auto apply
      changes: !reference [.infra-changedfiles]

format-infra-dev: { extends: [.infra-dev, .tf-format-base] }
validate-infra-dev: { extends: [.infra-dev, .tf-validate-base] }
tflint-infra-dev: { extends: [.infra-dev, .tf-tflint-base] }
tfsec-infra-dev: { extends: [.infra-dev, .tf-tfsec-base] }
plan-infra-dev: { extends: [.infra-dev, .tf-plan-base] }
apply-infra-dev:
  { extends: [.infra-dev, .tf-apply-base], dependencies: [plan-infra-dev] }

test-api-dev: { extends: [.api-dev, .test-base] }
check-build-api-dev: { extends: [.api-dev, .check-build-base] }
build-upload-api-dev: { extends: [.api-dev, .build-upload-base] }
deploy-api-dev: { extends: [.api-dev, .deploy-base] }

test-web-dev: { extends: [.web-dev, .test-base] }
check-build-web-dev: { extends: [.web-dev, .check-build-base] }
build-upload-web-dev: { extends: [.web-dev, .build-upload-base] }
deploy-web-dev: { extends: [.web-dev, .deploy-base] }

format-infra-prod: { extends: [.infra-prod, .tf-format-base] }
validate-infra-prod: { extends: [.infra-prod, .tf-validate-base] }
tflint-infra-prod: { extends: [.infra-prod, .tf-tflint-base] }
tfsec-infra-prod: { extends: [.infra-prod, .tf-tfsec-base] }
plan-infra-prod: { extends: [.infra-prod, .tf-plan-base] }
apply-infra-prod:
  { extends: [.infra-prod, .tf-apply-base], dependencies: [plan-infra-prod] }

test-api-prod: { extends: [.api-prod, .test-base] }
check-build-api-prod: { extends: [.api-prod, .check-build-base] }
build-upload-api-prod: { extends: [.api-prod, .build-upload-base] }
deploy-api-prod: { extends: [.api-prod, .deploy-base] }

test-web-prod: { extends: [.web-prod, .test-base] }
check-build-web-prod: { extends: [.web-prod, .check-build-base] }
build-upload-web-prod: { extends: [.web-prod, .build-upload-base] }
deploy-web-prod: { extends: [.web-prod, .deploy-base] }
