variable "aws_region" {
  type        = string
  description = "Region to lunch Infrastructure in"
}

variable "vpc_cidr" {
  type        = string
  description = "VPC CIDR to use"
}

variable "environment" {
  type        = string
  description = "Specify Environment, to be used in naming resources for an environment"
}

variable "app_name" {
  type        = string
  description = "A unique name to our application"
}

variable "ci_managed_definitions" {
  type        = bool
  default     = false
  description = "If there is a ci managing this task definitions, prevents recreating task definition and deploying."
}

variable "elb_account_id" {
  type        = string
  description = "ELB Account ID, get account ID from https://docs.aws.amazon.com/elasticloadbalancing/latest/application/enable-access-logging.html"
}

