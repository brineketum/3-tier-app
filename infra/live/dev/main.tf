terraform {
  backend "s3" {
    bucket = "kbrine--terraform-states--dev"
    key    = "kbrine-infra-dev"
    region = "us-east-1"
  }
}

provider "aws" {
  region = var.aws_region

  default_tags {
    tags = {
      CreatedBy   = "Terraform"
      Environment = var.environment
    }
  }
}

resource "aws_ecs_cluster" "app" {
  name = "${var.app_name}-${var.environment}"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_cluster_capacity_providers" "app_providers" {
  cluster_name = aws_ecs_cluster.app.name

  capacity_providers = ["FARGATE", "FARGATE_SPOT"]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}

module "networking" {
  source = "../../modules/networking"

  app_name    = var.app_name
  environment = var.environment
  aws_region  = var.aws_region
  vpc_cidr    = var.vpc_cidr
}

module "rds" {
  source = "../../modules/rds"

  app_name    = var.app_name
  pg_db_name  = "kbrine"
  environment = var.environment
  vpc_id      = module.networking.vpc_id

  random_password_length = 18

  db_engine_version = "13.7"
  depends_on        = [module.networking]
}

module "s3-logs" {
  source = "../../modules/s3-logs"

  environment    = var.environment
  elb_account_id = var.elb_account_id
}

module "app-api" {
  source = "../../modules/app-api"

  environment = var.environment
  vpc_id      = module.networking.vpc_id

  Tier     = "api"
  app_name = "${var.app_name}-api"

  ecs_cluster_id   = aws_ecs_cluster.app.id
  ecs_cluster_name = aws_ecs_cluster.app.name

  db_creds = {
    db_host = module.rds.db_host
    db_port = module.rds.db_port
    db_name = module.rds.db_name
    db_user = module.rds.db_user
  }
  db_pass_secret_arn = module.rds.db_root_pass_secret_arn

  ci_managed_definitions = var.ci_managed_definitions
  logs_bucket            = module.s3-logs.bucket_name

  depends_on = [module.rds]
}


module "app-web" {
  source = "../../modules/app-web"

  environment = var.environment
  vpc_id      = module.networking.vpc_id

  Tier     = "web"
  app_name = "${var.app_name}-web"

  ecs_cluster_id   = aws_ecs_cluster.app.id
  ecs_cluster_name = aws_ecs_cluster.app.name

  api_host_url = module.app-api.api_host_url

  ci_managed_definitions = var.ci_managed_definitions
  logs_bucket            = module.s3-logs.bucket_name
}
