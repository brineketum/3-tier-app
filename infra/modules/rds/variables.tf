variable "environment" {
  type        = string
  description = "Specify Environment, to be used in naming resources for an environment"
}

variable "make_db_public" {
  type        = bool
  default     = false
  description = "Make the db public for this environment"
}


variable "app_name" {
  type        = string
  description = "A unique name to our application"
}

variable "pg_db_name" {
  type        = string
  description = "A unique name to our db"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID to launch the DB in"
}

variable "pg_port" {
  type        = number
  default     = 5432
  description = "Port on which postgres will run"
}

variable "db_engine" {
  type        = string
  default     = "postgres"
  description = "DB engine type"
}

variable "db_engine_version" {
  type        = string
  description = "DB engine version"
}

variable "db_instance_class" {
  type        = string
  default     = "db.t4g.micro"
  description = "Instance class for the db"
}

variable "random_password_length" {
  type        = number
  default     = 16
  description = "Used to control reseting the master password, change value to reset"
}
