locals {
  username = "root"
}

locals {
  security_group_cidr = var.make_db_public ? "0.0.0.0/0" : data.aws_vpc.vpc.cidr_block
}

resource "aws_security_group" "allow_postgres" {
  name        = "terraform-managed-${var.app_name}-${var.environment}"
  description = "PostgreSQL Traffic"
  vpc_id      = data.aws_vpc.vpc.id

  ingress {
    description = "PostgreSQL access from within VPC"
    from_port   = var.pg_port
    to_port     = var.pg_port
    protocol    = "tcp"
    #tfsec:ignore:aws-vpc-no-public-ingress-sgr
    cidr_blocks = [local.security_group_cidr]
  }

  egress {
    description = "Allow all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    #tfsec:ignore:aws-vpc-no-public-egress-sgr
    cidr_blocks = [local.security_group_cidr]
  }

  tags = {
    Name = "terraform-managed-${var.app_name}-${var.environment}"
  }
}

#tfsec:ignore:aws-rds-no-public-db-access
module "this" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 5.4"

  identifier = "${var.app_name}-db-${var.environment}"

  engine            = var.db_engine
  engine_version    = var.db_engine_version
  instance_class    = var.db_instance_class
  allocated_storage = 5

  db_name  = var.pg_db_name
  port     = var.pg_port
  username = local.username

  random_password_length = var.random_password_length # used to control password resets

  iam_database_authentication_enabled = true

  publicly_accessible    = var.make_db_public
  vpc_security_group_ids = [aws_security_group.allow_postgres.id]

  maintenance_window      = "Mon:00:00-Mon:03:00"
  backup_window           = "03:00-06:00"
  backup_retention_period = 10 # days

  # Enhanced Monitoring - see example for details on how to create the role
  # by yourself, in case you don't want to create it automatically
  monitoring_interval             = "30"
  monitoring_role_name            = "terraform-managed-${var.app_name}-db-monitoring-role-${var.environment}"
  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]
  create_monitoring_role          = true

  performance_insights_enabled = true

  # DB subnet group
  multi_az               = true
  create_db_subnet_group = true
  db_subnet_group_tags = {
    networking = "db"
  }
  subnet_ids = data.aws_subnets.db.ids


  # Database Deletion Protection
  deletion_protection = true

  create_db_parameter_group = false
  create_db_option_group    = false
}

#tfsec:ignore:aws-ssm-secret-use-customer-key
resource "aws_secretsmanager_secret" "db_url" {
  name = "terraform-managed-${var.app_name}-db-url-${var.environment}"
}

#tfsec:ignore:general-secrets-no-plaintext-exposure
resource "aws_secretsmanager_secret_version" "db_url" {
  secret_id     = aws_secretsmanager_secret.db_url.id
  secret_string = "postgresql://${module.this.db_instance_username}:${module.this.db_instance_password}@${module.this.db_instance_address}:${module.this.db_instance_port}/${module.this.db_instance_name}"
}

#tfsec:ignore:aws-ssm-secret-use-customer-key
resource "aws_secretsmanager_secret" "db_root_pass" {
  name = "terraform-managed-${var.app_name}-db-root-pass-${var.environment}"
}

#tfsec:ignore:general-secrets-no-plaintext-exposure
resource "aws_secretsmanager_secret_version" "db_root_pass" {
  secret_id     = aws_secretsmanager_secret.db_root_pass.id
  secret_string = module.this.db_instance_password
}
