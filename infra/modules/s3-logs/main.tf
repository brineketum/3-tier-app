module "logs-bucket" {
  source = "../s3"

  resource_prefix    = "kbrine"
  resource_postfix   = var.environment
  resource_name      = "logs"
  versioning_enabled = false
}

# grant logging access to lbs
data "aws_iam_policy_document" "allow_access" {
  count = var.elb_account_id != null ? 1 : 0

  statement {
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.elb_account_id}:root"]
    }

    actions = ["s3:PutObject"]

    resources = [
      "arn:aws:s3:::${module.logs-bucket.bucket_name}/*",
    ]
  }
}

resource "aws_s3_bucket_policy" "allow_access" {
  count  = var.elb_account_id != null ? 1 : 0
  bucket = module.logs-bucket.bucket_name
  policy = data.aws_iam_policy_document.allow_access[0].json
}