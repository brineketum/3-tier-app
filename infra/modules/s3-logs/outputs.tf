output "bucket_name" {
  value = module.logs-bucket.bucket_name
}

output "bucket_arn" {
  value = module.logs-bucket.bucket_arn
}
