variable "environment" {
  type        = string
  description = "Specify Environment, to be used in naming resources for an environment"
}

variable "elb_account_id" {
  type        = string
  default     = null
  description = "ELB Account ID, get account ID from https://docs.aws.amazon.com/elasticloadbalancing/latest/application/enable-access-logging.html"
}
