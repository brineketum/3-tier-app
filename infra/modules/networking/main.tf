locals {
  az_names = [for name in slice(data.aws_availability_zones.available.names, 1, var.az_count + 1) :
    name
  ]
  az_shorts = [for name in slice(data.aws_availability_zones.available.names, 1, var.az_count + 1) :
    substr(name, -2, -1)
  ]
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.19.0"

  name = "${var.app_name}-vpc-${var.environment}"
  cidr = var.vpc_cidr

  azs              = local.az_names
  database_subnets = [for i in range(var.az_count) : cidrsubnet(var.vpc_cidr, 8, i)]
  private_subnets  = [for i in range(var.az_count) : cidrsubnet(var.vpc_cidr, 8, i + var.az_count)]
  public_subnets   = [for i in range(var.az_count) : cidrsubnet(var.vpc_cidr, 8, i + 2 * var.az_count)]

  enable_nat_gateway     = true
  single_nat_gateway     = false
  one_nat_gateway_per_az = true

  create_database_subnet_group           = true
  create_database_subnet_route_table     = var.make_db_public # allow public otherwise provision vpn
  create_database_internet_gateway_route = var.make_db_public # allow public otherwise provision vpn
  enable_dns_hostnames                   = true
  enable_dns_support                     = true

  database_subnet_tags = {
    networking = "db"
  }

  private_subnet_tags = {
    networking = "private"
    isPublic   = "false"
  }

  public_subnet_tags = {
    networking = "public"
    isPublic   = "true"
  }
}
