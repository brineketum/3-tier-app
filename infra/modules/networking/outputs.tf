output "vpc_id" {
  value       = module.vpc.vpc_id
  description = "ID of the created VPC"
}

output "az_names" {
  value       = local.az_names
  description = "Availability zone names used to create our vpc"
}

output "private_subnet_ids" {
  value       = module.vpc.private_subnets
  description = "List of private subnet Ids"
}
