output "lb_id" {
  value       = module.alb.lb_id
  description = "Load Balancer Id"
}
output "lb_dns_name" {
  value       = module.alb.lb_dns_name
  description = "DNS name of this lb"
}

output "lb_arn_suffix" {
  value       = module.alb.lb_arn_suffix
  description = "ALB target group suffix"
}

output "lb_target_group_arns" {
  value       = module.alb.target_group_arns
  description = "ALB target group arns"
}

output "lb_target_group_arn_suffixes" {
  value       = module.alb.target_group_arn_suffixes
  description = "ALB target group arn suffixes"
}