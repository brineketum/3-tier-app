module "security_group" {
  count = var.lb_type == "application" ? 1 : 0

  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "terraform-managed-${var.app_name}-alb-${var.environment}"
  description = "Security group for Public usage with ALB"
  vpc_id      = data.aws_vpc.main.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp"]
  egress_rules        = ["all-all"]

  tags = {
    Tier = var.Tier
  }
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 6.0"

  name = "${var.app_name}-lb-${var.environment}"

  load_balancer_type = var.lb_type
  internal           = var.lb_internal_facing

  vpc_id          = var.vpc_id
  subnets         = data.aws_subnets.subnet.ids
  security_groups = var.lb_type == "application" ? [module.security_group[0].security_group_id] : []

  access_logs = {
    bucket = var.access_logs_bucket
    prefix = var.Tier
  }

  target_groups = [
    {
      name             = "${var.app_name}-tg-${var.environment}"
      backend_protocol = var.lb_type == "application" ? "HTTP" : "TCP"
      backend_port     = var.app_port
      target_type      = "ip"

      health_check = {
        path = var.health_check_path
      }
    }
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = var.lb_type == "application" ? "HTTP" : "TCP"
      target_group_index = 0
    }
  ]

  tags = {
    Tier = var.Tier
  }

  target_group_tags = {
    Tier = var.Tier
  }

  http_tcp_listener_rules_tags = {
    Tier = var.Tier
  }

  http_tcp_listeners_tags = {
    Tier = var.Tier
  }
}
