data "aws_vpc" "main" {
  id = var.vpc_id
}

data "aws_caller_identity" "current" {}

data "aws_subnets" "subnet" {
  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }
  tags = {
    networking = var.lb_internal_facing ? "private" : "public"
  }
}
