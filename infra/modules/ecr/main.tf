#tfsec:ignore:aws-ecr-repository-customer-key
#tfsec:ignore:aws-ecr-enforce-immutable-repository
resource "aws_ecr_repository" "app" {
  name                 = var.app_name
  image_tag_mutability = "MUTABLE" # allow so we can always set latest

  image_scanning_configuration {
    scan_on_push = true
  }

  tags = {
  }
}

# allow for sharing this ecr repository
resource "aws_ecr_registry_policy" "app_ecr_policy" {
  count = length(var.allow_ecr_from_account_ids) == 0 ? 0 : 1 # only pass in base env, when a target env's account isn't same as ecr env account

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Sid    = "allowAccess",
        Effect = "Allow",
        Principal = {
          "AWS" : [
            for account_id in var.allow_ecr_from_account_ids : "arn:${data.aws_partition.current.partition}:iam::${account_id}:root"
          ]
        },
        Action = [
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "ecr:BatchCheckLayerAvailability",
        ],
        Resource = [
          "arn:${data.aws_partition.current.partition}:ecr:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:repository/${aws_ecr_repository.app.name}"
        ]
      }
    ]
  })
}
