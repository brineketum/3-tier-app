variable "app_name" {
  type        = string
  description = "A unique name to our application"
}

variable "allow_ecr_from_account_ids" {
  type        = list(string)
  default     = []
  description = "Only pass in base env, when a target env's account isn't same as ecr env account"
}
