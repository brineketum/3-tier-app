variable "vpc_id" {
  type        = string
  description = "VPC ID to launch the ecs in"
}

variable "environment" {
  type        = string
  description = "Specify Environment, to be used in naming resources for an environment"
}

variable "Tier" {
  type        = string
  description = "Module's Tier"
}

variable "app_name" {
  type        = string
  description = "A unique name to our application"
}

variable "app_image_tag" {
  type        = string
  default     = "latest"
  description = "Tag to be pulled from URL"
}

variable "app_image_ecr_url" {
  type        = string
  default     = null
  description = "Image URL for pulling image, if null will create ecr repo. Used for other environments"
}

variable "allow_ecr_from_account_ids" {
  type        = list(string)
  default     = []
  description = "Only pass in base env, when a target env's account isn't same as ecr env account"
}

variable "ecs_cluster_id" {
  type        = string
  description = "Cluster ID to launch the ecs in"
}

variable "ecs_cluster_name" {
  type        = string
  description = "Cluster Name of our working cluster"
}

variable "api_host_url" {
  type        = string
  description = "URL used for connecting to api"
}

variable "ci_managed_definitions" {
  type        = bool
  default     = false
  description = "If there is a ci managing this task definitions, prevents recreating task definition and deploying."
}

variable "logs_bucket" {
  type        = string
  description = "logs bucket to send logs to"
}