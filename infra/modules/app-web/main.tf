locals {
  app_port = 8080
}


module "web-ecr" {
  count = var.environment == "dev" ? 1 : 0 # only create registory in dev

  source = "../ecr"

  app_name                   = var.app_name
  allow_ecr_from_account_ids = var.allow_ecr_from_account_ids
}

module "web-alb" {
  source = "../alb"

  environment = var.environment
  Tier        = var.Tier
  vpc_id      = var.vpc_id
  app_name    = var.app_name
  app_port    = local.app_port

  lb_type            = "application"
  lb_internal_facing = false

  access_logs_bucket = var.logs_bucket
}

module "web-cloudfront" {
  source  = "terraform-aws-modules/cloudfront/aws"
  version = "~> 2.9"

  price_class = "PriceClass_200" # Regional Distribution
  comment     = "Cloudfront for Web distribution"

  origin = {
    web_alb = {
      domain_name = module.web-alb.lb_dns_name
      origin_id   = module.web-alb.lb_dns_name
      custom_origin_config = {
        http_port              = 80
        https_port             = 443
        origin_protocol_policy = "http-only"
        origin_ssl_protocols   = ["TLSv1.2"]
      }
    }
  }

  logging_config = {
    include_cookies = true
    bucket          = "${var.logs_bucket}.s3.amazonaws.com"
    prefix          = var.Tier
  }

  default_cache_behavior = {
    target_origin_id       = module.web-alb.lb_dns_name
    viewer_protocol_policy = "allow-all"

    allowed_methods = ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
    cached_methods  = ["GET", "HEAD", "OPTIONS"]
    compress        = true
    query_string    = true
    cookies         = true
  }

  tags = {
    Tier = var.Tier
    name = var.app_name
  }

  depends_on = [module.web-alb]
}


module "web-ecs-service" {
  source = "../ecs-service"

  environment = var.environment

  cluster_id             = var.ecs_cluster_id
  cluster_name           = var.ecs_cluster_name
  vpc_id                 = var.vpc_id
  app_name               = var.app_name
  app_port               = local.app_port
  image_url              = var.app_image_ecr_url != null ? var.app_image_ecr_url : module.web-ecr[0].repository_url
  image_tag              = var.app_image_tag
  ci_managed_definitions = var.ci_managed_definitions

  autoscaling_settings = {
    min_capacity         = 1
    max_capacity         = 4
    target_request_value = 100
  }

  alb_arn_suffix              = module.web-alb.lb_arn_suffix
  alb_target_group_arn        = module.web-alb.lb_target_group_arns[0]
  alb_target_group_arn_suffix = module.web-alb.lb_target_group_arn_suffixes[0]

  task_environment_variables = [
    { name = "HOST", value = "0.0.0.0" },
    { name = "PORT", value = local.app_port },
    { name : "API_HOST", value : var.api_host_url },
  ]

  depends_on = [
    module.web-alb
  ]
}
