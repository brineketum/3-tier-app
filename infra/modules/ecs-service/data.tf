data "aws_region" "current_region" {

}

data "aws_vpc" "vpc" {
  id = var.vpc_id
}

data "aws_subnets" "private" {
  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }
  tags = {
    networking = "private"
    isPublic   = "false"
  }
}

/*
  Using this hack to accomplish syncing between existing definitions managed by some ci.

  The ci is supposed to modify a single
*/
data "aws_ecs_task_definition" "task" {
  count           = var.ci_managed_definitions ? 1 : 0
  task_definition = "${var.app_name}-${var.environment}"
}


data "aws_ecs_container_definition" "task" {
  count           = var.ci_managed_definitions ? 1 : 0
  task_definition = tolist(data.aws_ecs_task_definition.task)[0].id
  container_name  = var.app_name
}
