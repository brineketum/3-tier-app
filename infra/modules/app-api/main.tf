locals {
  app_name = var.app_name
  app_port = 8080
}

module "api-ecr" {
  count = var.environment == "dev" ? 1 : 0 # only create registory in dev

  source = "../ecr"

  app_name                   = local.app_name
  allow_ecr_from_account_ids = var.allow_ecr_from_account_ids
}


module "api-alb" {
  source = "../alb"

  environment       = var.environment
  Tier              = var.Tier
  vpc_id            = var.vpc_id
  app_name          = local.app_name
  app_port          = local.app_port
  health_check_path = "/api/status"

  lb_type            = "application"
  lb_internal_facing = true

  access_logs_bucket = var.logs_bucket
}

module "api-ecs-service" {
  source = "../ecs-service"

  environment = var.environment

  cluster_id             = var.ecs_cluster_id
  cluster_name           = var.ecs_cluster_name
  vpc_id                 = var.vpc_id
  app_name               = local.app_name
  app_port               = local.app_port
  image_url              = var.app_image_ecr_url != null ? var.app_image_ecr_url : module.api-ecr[0].repository_url
  image_tag              = var.app_image_tag
  ci_managed_definitions = var.ci_managed_definitions

  autoscaling_settings = {
    min_capacity         = 1
    max_capacity         = 4
    target_request_value = 100
  }

  alb_arn_suffix              = ""#module.api-alb.lb_arn_suffix
  alb_target_group_arn        = ""#module.api-alb.lb_target_group_arns[0]
  alb_target_group_arn_suffix = ""#module.api-alb.lb_target_group_arn_suffixes[0]

  task_environment_variables = [
    { name = "HOST", value = "0.0.0.0" },
    { name = "PORT", value = local.app_port },
    { name : "DB", value : var.db_creds.db_name },
    { name : "DBUSER", value : var.db_creds.db_user },
    { name : "DBHOST", value : var.db_creds.db_host },
    { name : "DBPORT", value : tostring(var.db_creds.db_port) }
  ]

  task_secret_environment_variables = [
    { name = "DBPASS", valueFrom = var.db_pass_secret_arn }
  ]

  depends_on = [
    module.api-alb
  ]
}
