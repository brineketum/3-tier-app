output "api_host_url" {
  value       = "http://${module.api-alb.lb_dns_name}"
  description = "DNS name, url to access api"
}