
variable "resource_prefix" {
  type        = string
  description = "S3 reource identifyer prefix"
}

variable "resource_postfix" {
  type        = string
  description = "This should be the environment name"
}

variable "resource_name" {
  type        = string
  description = "Resource name, should start with team/project identifyer"
}

variable "versioning_enabled" {
  type        = bool
  default     = true
  description = "Enable versioning of objects within bucket"
}
