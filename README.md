# Sample 3tier app
This repo contains code for a Node.js multi-tier application.

The application overview is as follows

```
web <=> api <=> db
```

The folders `web` and `api` respectively describe how to install and run each app.


# How the Pipeline Works

The pipeline picks up changes at specific directories and runs specific Jobs. Specific tiers can be runned by using the RUN_TIERS environment variable. It will besically invoke that tier only.

- Deployments (ECS, terraform) are only done on default branches (main).

- The Pipeline uses a specially built docker image. It will be pushed to gitlab registory and will be used to run the all jobs. You can always manually invoke building new by running the pipeline with `BUILD_CI_IMAGE` set to `true`.
- Architecture Diagram, found in `architecture.drawio`

## Features

- GitLab [Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/) Integration
- Terraform Linting
- Terraform Static Security analysis
- Terraform Reports Presentation in MRs
- Other Unit Test Reports Artificats
- Terraform Validation and Format checks
- Code in all Tiers testing by calling `npm test`
- Build and deployment of Images of all Tiers to ECR

## How to Run

**Notes**

- The current setup uses `us-east-1` to deploy infrastructure. You can aways change that in Terraform vars.
- Make sure to create the remote s3 bucket with [appropriate permissions](https://www.terraform.io/language/settings/backends/s3#s3-bucket-permissions).

### Environment Variables

You will need the following CI Environment variables set up

#### AWS Authentication

- **AWS_REGION_DEV**: Specify the dev region you are deploying to
- **AWS_ACCESS_KEY_ID**: Access Key to use for deployments
- **AWS_SECRET_ACCESS_KEY**: Access Secret for use for deployments

### Pipeline Variables

- **RUN_ENVS**: "The deployment target. Change this variable to 'dev' or 'qa' or deploy multiple 'dev qa' if needed."
- **RUN_TIERS**: "The deployment tier, Change this variable to 'api', 'infra', 'web' or multiple 'api infra web' if needed."
- **TIER_IMAGE_TAG**: "A tier image tag to push to registry, could be same as deploy, so it's deployed."
- **BUILD_TIER_IMAGE**: "If tier image should be built. Otherwise would deploy tag or ci."
- **BUILD_CI_IMAGE**: "If set to 'yes', will build a new CI Runner Image, otherwise would not.
