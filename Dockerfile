FROM node:16-alpine3.15

ENV TFSEC_VERSION=1.28.1

RUN apk update && \
  apk add --no-cache git openssl curl docker bash unzip jq

# Required to run tfswitch in alpine @see https://stackoverflow.com/a/68491669/7387312
RUN apk add --no-cache libc6-compat

RUN cd /tmp
# install terraform related deps
RUN curl -L https://raw.githubusercontent.com/warrensbox/terraform-switcher/release/install.sh | bash && \
  curl -s https://raw.githubusercontent.com/terraform-linters/tflint/master/install_linux.sh | bash && \
  curl -L -o tfsec https://github.com/aquasecurity/tfsec/releases/download/v${TFSEC_VERSION}/tfsec-linux-amd64 && \
  chmod +x ./tfsec && \
  mv ./tfsec /usr/local/bin/tfsec

# Pre-install latest terraform version
RUN tfswitch -u

# install aws cli
RUN apk add --no-cache \
  python3 \
  py3-pip \
  && pip3 install --upgrade pip \
  && pip3 install --no-cache-dir \
  awscli

RUN aws --version

WORKDIR /ci

CMD /bin/bash